import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { Subscription } from 'rxjs/Subscription';
import { Push, PushObject, PushOptions } from '@ionic-native/push';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit {
  loginDetails: any = {};
  schedules: any[] = [];
  token: any;
  pushnotify: any;
  connected: Subscription;
  disconnected: Subscription;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpUrl: ApiCallerProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public storage: Storage,
    private network: Network,
    private push: Push,
  ) {
    console.log("login details=> " + localStorage.getItem('details'));
    this.loginDetails = JSON.parse(localStorage.getItem('details'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  ionViewDidEnter() {
    this.connected = this.network.onConnect().subscribe(data => {
      this.getSchedules();
    }, error => console.log(error));

    this.disconnected = this.network.onDisconnect().subscribe(data => {
      this.displayNetworkUpdate(data.type);
    }, error => console.log(error));
  }

  ionViewWillLeave() {
    this.connected.unsubscribe();
    this.disconnected.unsubscribe();
  }

  displayNetworkUpdate(connectionState: string) {
    // let networkType = this.network.type;
    this.toastCtrl.create({
      // message: `You are now ${connectionState} via ${networkType}`,
      message: `You are now ${connectionState}`,
      duration: 3000,
      position: 'bottom'
    }).present();
  }

  ngOnInit() {
    this.getSchedules();
  }

  getSchedules() {
    this.httpUrl.startLoading().present();
    this.httpUrl.getSchedules(this.loginDetails._id)
      .subscribe(data => {
        this.httpUrl.stopLoading();
        console.log("data=> ", data);
        if (data.length > 0) {
          this.innerFunc(data);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Schedules not found..',
            duration: 1500,
            position: 'middle'
          })
          toast.present();
        }
        this.addPushNotify();
      },
        err => {
          this.httpUrl.stopLoading();
          console.log("error got=> ", err)
        });
  }

  innerFunc(sched) {
    let that = this;
    that.schedules = [];
    var i = 0, howManyTimes = sched.length;
    function f() {
      that.schedules.push({
        'scheduleschools': sched[i].scheduleschools,
        '_id': sched[i]._id
      });
      if (sched[i]._id != null && sched[i]._id != undefined) {
        that.getTripStat(sched[i]);
        
      } else {
        that.schedules[that.schedules.length - 1].tripStat = 'N/A'
      }
      console.log("finally data: " + that.schedules);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 200);
      }
    } f();
  }

  getTripStat(data) {
    let that = this;
    var postData = {
      "_find": {
        "createdAt": {
          "$gte": {
            "_eval": "dayStart",
            "value": new Date().toISOString()
          }
        },
        "ScheduleSchool": {
          "_eval": "Id",
          "value": data._id
        }
      }
    }

    that.httpUrl.tripStatusCall(postData)
      .subscribe(respData => {
        debugger
        console.log('response data', respData)
        if (respData.length == 0) {
          that.schedules[that.schedules.length - 1].tripStat = 'Trip not started yet';
        } else {
          if (respData[0].pickup == "START") {
            that.schedules[that.schedules.length - 1].tripStat = 'Pickup Ongoing';
          } else {
            if (respData[0].drop == "START") {
              that.schedules[that.schedules.length - 1].tripStat = 'Drop Ongoing';
            } else {
              if (respData[0].pickup == "END") {
                that.schedules[that.schedules.length - 1].tripStat = 'Pickup Completed';
              } else {
                if (respData[0].drop == "END") {
                  that.schedules[that.schedules.length - 1].tripStat = 'Drop Completed';
                }
              }
            }
          }
        }
      });

  }

  timeoutAlert() {
    let alerttemp = this.alertCtrl.create({
      message: "Server is taking much time to respond. Please try in some time.",
      buttons: [{
        text: 'Okay',
        handler: () => {
          this.navCtrl.setRoot("TabsPage");
        }
      }]
    });
    alerttemp.present();
  }

  doRefresh(refresher) {
    // console.log('Begin async operation', refresher);
    this.getSchedules();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 200);
  }

  pushSetup() {
    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
        icon: 'safety365',
        iconColor: 'red'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);

    pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log("device token => " + registration.registrationId);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId)
        this.storage.set("DEVICE_TOKEN", registration.registrationId);
        this.addPushNotify();
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
    });
  }

  addPushNotify() {
    // console.log("push notify click")
    this.storage.get("DEVICE_TOKEN").then((res) => {
      if (res) {
        console.log("device token=> " + JSON.stringify(res));
        this.token = res;
        var pushdata = {
          "uid": this.loginDetails._id,
          "token": this.token,
          "os": "android",
          "appName": "Safety365"
        }
        // console.log("pushdata=> " + JSON.stringify(pushdata));
        this.httpUrl.pushnotifyCall(pushdata)
          .subscribe(data => {
            this.pushnotify = data.message;
            console.log("data of push=> " + JSON.stringify(data));
          },
            error => {
              console.log(error);
            });
      } else {
        this.pushSetup();
      }
    });

    // debugger;
  }

  showTrip(data) {
    console.log('show Trip', data)

    var postData = {
      "_find": {
        "createdAt": {
          "$gte": {
            "_eval": "dayStart",
            "value": new Date().toISOString()
          }
        },
        "ScheduleSchool": {
          "_eval": "Id",
          "value": data._id
        }
      }
    }
    this.httpUrl.startLoading().present();
    this.httpUrl.tripStatusCall(postData)
      .subscribe(respData => {

        console.log('response data', respData)
        this.httpUrl.stopLoading();
        debugger
        if (respData.length == 0) {
          const toast = this.toastCtrl.create({
            message: 'This trip is yet to start.. Please check afetr some time. Thank you!',
            duration: 3000,
            position: 'bottom'
          })
          toast.present();
        } else {
          if (respData[0].pickup == "START" || respData[0].drop == "START") {
            this.navCtrl.push("LiveSingleDevice", {
              imei_ID: data.scheduleschools.schoolVehicles.device.Device_ID,
              param: respData[0],
              data: data
            });
          } else {
            if (respData[0].pickup == "END" || respData[0].drop == "END") {
              this.navCtrl.push("HistoryMapPage", {
                imei_ID: data.scheduleschools.schoolVehicles.device.Device_ID,
                param: respData[0]
              });
            }
          }
        }
      },
        err => {
          this.httpUrl.stopLoading();
          console.log("error occured=> ", err)
        });
  }

}
