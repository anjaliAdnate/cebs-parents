import { Component } from '@angular/core';
import { Events, IonicPage } from 'ionic-angular';
import * as io from 'socket.io-client';   // with ES6 import
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'HomePage';
  tab2Root = 'NotificationPage';
  tab3Root = 'ReportsPage';
  tab4Root = 'SettingsPage';
  tab5Root = 'ProfilePage';

  public cartCount: number = 0;

  socket: any;
  notData: any = [];
  userdetails: any;

  constructor(public events: Events, public apiCall: ApiCallerProvider) {
    this.events.subscribe('cart:updated', (count) => {
      this.cartCount = count;
    });

    this.userdetails = JSON.parse(localStorage.getItem('details'));
    // this.getnotifications();
    this.socket = io('http://www.thingslab.in/sbNotifIO', {
      transports: ['websocket', 'polling']
    });

    this.socket.on('connect', () => {
      console.log('IO Connected tabs');
      console.log("socket connected tabs", this.socket.connected)
    });
    
    this.socket.on(this.userdetails._id, (msg) => {
      this.notData.push(msg);
      this.cartCount += 1;
      console.log("tab notice data=> " + this.notData)
    })

  }

  // getnotifications() {
  //   this.apiCall.getRecentNotifCall(this.userdetails._id)
  //     .subscribe(data => {
  //       this.notData = data;
  //     },
  //       error => {
  //         console.log(error);
  //       });
  // }


}
