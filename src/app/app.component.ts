import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, AlertController, App, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { ApiCallerProvider } from '../providers/api-caller/api-caller';
import { Uid } from '@ionic-native/uid';
import { Diagnostic } from '../../node_modules/@ionic-native/diagnostic';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-main-page',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  islogin: any = {};
  token: any;
  isDealer: any;
  notfiD: boolean;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    private push: Push,
    public alertCtrl: AlertController,
    public app: App,
    public apiCall: ApiCallerProvider,
    public toastCtrl: ToastController,
    private tts: TextToSpeech,
    private uid: Uid,
    private diagnostic: Diagnostic,
    public storage: Storage
  ) {

    platform.ready().then(() => {
      this.getPermission();
      statusBar.styleDefault();
      statusBar.hide();
      this.splashScreen.hide();

      platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];
        let activeView = nav.getActive();
          if (nav.canGoBack()) { // CHECK IF THE USER IS IN THE ROOT PAGE.
            nav.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
          } else {
            platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
          }
        // }
      });
    });

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.initializeApp();

    if (localStorage.getItem("LOGGED_IN")) {
      this.rootPage = 'TabsPage';
    } else {
      this.rootPage = 'LoginPage';
    }

  }

  getPermission() {
    this.diagnostic.getPermissionAuthorizationStatus(this.diagnostic.permission.READ_PHONE_STATE).then((status) => {
      if (status != this.diagnostic.permissionStatus.GRANTED) {
        this.diagnostic.requestRuntimePermission(this.diagnostic.permission.READ_PHONE_STATE).then((data) => {
          var imei = this.uid.IMEI;
        })
      }
      else {
        console.log("We have the permission");
        localStorage.setItem("IMEI", this.uid.IMEI);
        localStorage.setItem("UUID", this.uid.UUID);
      }
    }, (statusError) => {
      console.log(`statusError=> ` + statusError);
    });
  }

  pushNotify() {
    let that = this;
    that.push.hasPermission()                       // to check if we have permission
      .then((res: any) => {
        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
          that.pushSetup();
        } else {
          that.pushSetup();
          console.log('We do not have permission to send push notifications');
        }
      });
  }

  pushSetup() {
    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
        icon: 'safety365',
        iconColor: 'red'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);


    pushObject.on('notification').subscribe((notification: any) => {
      // if (localStorage.getItem("notifValue") != null) {
      //   if (localStorage.getItem("notifValue") == 'true') {
      this.tts.speak(notification.message)
        .then(() => console.log('Success'))
        .catch((reason: any) => console.log(reason));
      //   }
      // }

      if (notification.additionalData.foreground) {
        const toast = this.toastCtrl.create({
          message: notification.message,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log("device token => " + registration.registrationId);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId)
        this.storage.set("DEVICE_TOKEN", registration.registrationId);
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
    });
  }

  initializeApp() {
    let that = this;
    that.platform.ready().then(() => {
      that.pushNotify();
    });
  }
}
